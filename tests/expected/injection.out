-- This test cannot be run in a single transaction
-- because it will produce expected errors
CREATE EXTENSION IF NOT EXISTS anon CASCADE;
-- Sample Table
CREATE TABLE a (
    i SERIAL,
    d TIMESTAMP,
    x INTEGER
);
--
-- random_phone
--
--returns TRUE
SELECT anon.random_phone('11; SELECT 0;') LIKE '11; SELECT 0;%';
 ?column? 
----------
 t
(1 row)

--
-- add_noise_on_numeric_column
--
-- returns 'invalid name syntax'
SELECT anon.add_noise_on_numeric_column('a; SELECT 1','x',0.5);
ERROR:  invalid name syntax
LINE 1: SELECT anon.add_noise_on_numeric_column('a; SELECT 1','x',0....
                                                ^
-- returns a WARNING and FALSE
SELECT anon.add_noise_on_numeric_column('a','x; SELECT 1',0.5) IS FALSE;
WARNING:  Column 'x; SELECT 1' is not present in table 'a'.
 ?column? 
----------
 t
(1 row)

--
-- add_noise_on_datetime_column
--
-- returns 'invalid name syntax'
SELECT anon.add_noise_on_datetime_column('a; SELECT 1','d','2 days');
ERROR:  invalid name syntax
LINE 1: SELECT anon.add_noise_on_datetime_column('a; SELECT 1','d','...
                                                 ^
-- returns a WARNING and FALSE
SELECT anon.add_noise_on_datetime_column('a','d; SELECT 1','2 days') IS FALSE;
WARNING:  Column 'd; SELECT 1' is not present in table 'a'.
 ?column? 
----------
 t
(1 row)

-- returns 'invalid name syntax'
SELECT anon.add_noise_on_datetime_column('a','d','2 days; SELECT 1');
ERROR:  invalid input syntax for type interval: "2 days; SELECT 1"
LINE 1: SELECT anon.add_noise_on_datetime_column('a','d','2 days; SE...
                                                         ^
--
-- shuffle_column
--
-- returns 'invalid name syntax'
SELECT anon.shuffle_column('a; SELECT 1','x','i');
ERROR:  invalid name syntax
LINE 1: SELECT anon.shuffle_column('a; SELECT 1','x','i');
                                   ^
-- returns a WARNING and FALSE
SELECT anon.shuffle_column('a','x; SELECT 1','i') IS FALSE;
WARNING:  Column 'x; SELECT 1' is not present in table 'a'.
 ?column? 
----------
 t
(1 row)

-- returns a WARNING and FALSE
SELECT anon.shuffle_column('a','x','i; SELECT 1') IS FALSE;
WARNING:  Column 'i; SELECT 1' is not present in table 'a'.
 ?column? 
----------
 t
(1 row)

--
-- load
--
-- returns a WARNING and FALSE
SELECT anon.load('base/''; CREATE TABLE inject_via_load (i int);--') IS FALSE;
NOTICE:  Data file 'base/'; CREATE TABLE inject_via_load (i int);--/identifiers_category.csv' is not present. Skipping.
NOTICE:  Data file 'base/'; CREATE TABLE inject_via_load (i int);--/identifier_fr_FR.csv' is not present. Skipping.
NOTICE:  Data file 'base/'; CREATE TABLE inject_via_load (i int);--/identifier_en_US.csv' is not present. Skipping.
NOTICE:  Data file 'base/'; CREATE TABLE inject_via_load (i int);--/city.csv' is not present. Skipping.
NOTICE:  Data file 'base/'; CREATE TABLE inject_via_load (i int);--/company.csv' is not present. Skipping.
NOTICE:  Data file 'base/'; CREATE TABLE inject_via_load (i int);--/email.csv' is not present. Skipping.
NOTICE:  Data file 'base/'; CREATE TABLE inject_via_load (i int);--/first_name.csv' is not present. Skipping.
NOTICE:  Data file 'base/'; CREATE TABLE inject_via_load (i int);--/iban.csv' is not present. Skipping.
NOTICE:  Data file 'base/'; CREATE TABLE inject_via_load (i int);--/last_name.csv' is not present. Skipping.
NOTICE:  Data file 'base/'; CREATE TABLE inject_via_load (i int);--/siret.csv' is not present. Skipping.
NOTICE:  Data file 'base/'; CREATE TABLE inject_via_load (i int);--/lorem_ipsum.csv' is not present. Skipping.
 ?column? 
----------
 t
(1 row)

SELECT COUNT(*) = 0
FROM pg_tables
WHERE tablename='inject_via_load';
 ?column? 
----------
 t
(1 row)

--
-- Dynamic Masking
--
-- returns TRUE
SELECT anon.start_dynamic_masking(
  'public',
  'foo; CREATE TABLE inject_via_init (i int);--'
);
 start_dynamic_masking 
-----------------------
 t
(1 row)

SELECT COUNT(*) = 0
FROM pg_tables
WHERE tablename='inject_via_init';
 ?column? 
----------
 t
(1 row)

--
-- Masking Rule Syntax
--
SECURITY LABEL FOR anon ON COLUMN a.x
IS 'MASKED WITH VALUE foo; CREATE TABLE inject_via_rule (i int);--';
ERROR:  syntax error at or near ";"
LINE 1: ...ject_via_init (i int);--".a AS SELECT i,d,CAST(foo; CREATE T...
                                                             ^
QUERY:  CREATE OR REPLACE VIEW "foo; CREATE TABLE inject_via_init (i int);--".a AS SELECT i,d,CAST(foo; CREATE TABLE inject_via_rule (i int);-- AS integer) AS x FROM public.a
CONTEXT:  PL/pgSQL function anon.mask_create_view(oid) line 3 at EXECUTE
SQL function "mask_update" statement 2
SQL statement "SELECT anon.mask_update()"
PL/pgSQL function anon.mask_trigger() line 5 at PERFORM
SELECT COUNT(*) = 0
FROM pg_tables
WHERE tablename='inject_via_rule';
 ?column? 
----------
 t
(1 row)

-- CLEAN UP
DROP TABLE a CASCADE;
NOTICE:  drop cascades to view "foo; CREATE TABLE inject_via_init (i int);--".a
DROP EXTENSION anon CASCADE;
NOTICE:  drop cascades to event trigger anon_mask_update
